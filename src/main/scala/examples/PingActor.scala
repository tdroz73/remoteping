package examples

import akka.actor.{Actor, Props}
import com.typesafe.scalalogging.LazyLogging
import examples.PingPongProtocol.{Ping, Pong, Start}

object PingActor {
  def props = Props[PingActor]
}

object PingPongProtocol {
  case object Start
  case class Ping(who : String)
  case class Pong(who : String, count : Int)
}

class PingActor extends Actor with LazyLogging {
  private val maxPing = 100

  private val pongActor = context.actorSelection("akka.tcp://PongSystem@127.0.0.1:2552/user/pongActor")

  override def preStart(): Unit = logger.info(s"${self.path} - I'm alive!")

  override def postStop(): Unit = logger.info(s"${self.path} - I'm dead. :(")

  override def receive: Receive = {
    case Start =>
      logger.info("Starting PingActor!")
      pongActor ! Ping("Some_0")

    case Pong(who, count) =>
      logger.info(s"Ponged for $who - total: $count pongs!")
      if (count < maxPing) {
        sender ! Ping(s"Some_$count")
      } else {
        context.stop(self)
      }

    case unexpected => logger.warn(s"Hmmm, we got something odd... $unexpected")
  }
}
